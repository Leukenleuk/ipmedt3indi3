window.onload = () => {
  const resetPokemon = document.getElementById('js--box');
  const logo = document.getElementById('js--afbeelding');
  const camera = document.getElementById('js--camera');
  const textstart = document.getElementById('js--textstart');
  const texthelft = document.getElementById('js--texthelft');
  const texteind = document.getElementById('js--texteind');

  //vechtknoppen
  const menu = document.getElementById('js--menu');
  const actie0 = document.getElementById('js--actie0');
  const actie1 = document.getElementById('js--actie1');
  const actie2 = document.getElementById('js--actie2');
  const actie3 = document.getElementById('js--actie3');

  //verplaatsen
  const bulbasaur = document.getElementsByClassName('js--bulbasaur');
  const vlak = document.getElementsByClassName("js--vlak");
  const charmander = document.getElementsByClassName('js--charmander');
  const squirtle = document.getElementsByClassName('js--squirtle');

  //hussel van de random pokemon's
  const scene = document.getElementById("js--scene");
  const hussel = document.getElementsByClassName("js--hussel");
  const pokemonImages = document.getElementsByClassName('js--pokemon-picture');
  const pokemonGevecht = document.getElementsByClassName('js--pokemon-gevecht');

  let hold = null;
  let pokemon = '';
  let urlL = [];
  let currentPokemon = null;
  let actie = '';

  //Het laten oppakken van een pokemon
  function addListeners() {
    for (let i = 0; i <pokemonImages.length; i++){
      pokemonImages[i].addEventListener('click', function(evt){
        if (hold == null) {
          currentPokemon = i;
          hold="box";
          camera.innerHTML += '<a-image id="js--hold" class="js--pokemon-picture" src="'+ urlL[i] +'" position="1 -1 -1"></a-image>';
          pokemon += urlL;
          this.remove();
          texthelft.setAttribute("value", "Zet je tegenstander op het linker vlak");
          texthelft.setAttribute("position", "-1.5 3 -4")
        }
      });
    }
    for (let i = 0; i < charmander.length; i++){
      charmander[i].addEventListener('click', function(evt){
        if (hold == null) {
          hold="box";
          camera.innerHTML += '<a-image id="js--hold" class="js--charmander" src="img/charmander.png" position="1 -1 -1"></a-image>';
          pokemon += 'charmander';
          this.remove();
          textstart.setAttribute("value", "Kijk weer terug naar links");
          texthelft.setAttribute("value", "Zet je pokemon neer op het rechter vlak");
          texthelft.setAttribute("position", "-1.80 3 -4");
          let p = fetch("https://pokeapi.co/api/v2/pokemon/" + 4)
            .then((data) => {
                return data.json();
            })
            .then((response) => {
              for (let j = 0; j <4; i++){
                actie += response.moves[i].move.name + " ";
                j += 1;
              }
            });
        }
      });
    }
    for (let i = 0; i < bulbasaur.length; i++){
      bulbasaur[i].addEventListener('click', function(evt){
        if (hold == null) {
          hold="box";
          camera.innerHTML += '<a-image id="js--hold" class="js--bulbasaur" src="img/bulbasaur.png" position="1 -1 -1"></a-image>';
          pokemon += 'bulbasaur';
          this.remove();
          textstart.setAttribute("value", "Kijk weer terug naar links");
          texthelft.setAttribute("value", "Zet je pokemon neer op het rechter vlak");
          texthelft.setAttribute("position", "-1.80 3 -4");
          let p = fetch("https://pokeapi.co/api/v2/pokemon/" + 1)
            .then((data) => {
                return data.json();
            })
            .then((response) => {
              for (let j = 0; j <4; i++){
                actie += response.moves[i].move.name + " ";
                j += 1;
              }
            });
        }
      });
    }
    for (let i = 0; i < squirtle.length; i++){
      squirtle[i].addEventListener('click', function(evt){
        if (hold == null) {
          hold="box";
          camera.innerHTML += '<a-image id="js--hold" class="js--squirtle" src="img/squirtle.png" position="1 -1 -1"></a-image>';
          pokemon += 'squirtle';
          this.remove();
          textstart.setAttribute("value", "Kijk weer terug naar links");
          texthelft.setAttribute("value", "Zet je pokemon neer op het rechter vlak");
          texthelft.setAttribute("position", "-1.80 3 -4");
          let p = fetch("https://pokeapi.co/api/v2/pokemon/" + 7)
            .then((data) => {
                return data.json();
            })
            .then((response) => {
              for (let j = 0; j <4; i++){
                actie += response.moves[i].move.name + " ";
                j += 1;
              }
            });
        }
      });
    }
  }

  addListeners();
  //het terug zetten van een pokemon
  for (let i = 0; i < vlak.length; i++){
      vlak[i].addEventListener('click', function(evt){
        if (hold == "box") {
          let box = document.createElement("a-image");
          if (pokemon == 'bulbasaur' || pokemon == 'squirtle'){
            box.setAttribute("class", "js--" + pokemon + " clickable");
            box.setAttribute("src", "img/" + pokemon + ".png");
            texthelft.setAttribute("value", "Kijk dan weer naar links");
            texthelft.setAttribute("position", "-1 3 -4");
          }
          else if (pokemon == 'charmander') {
            box.setAttribute("class", "js--" + pokemon + " clickable");
            box.setAttribute("src", "img/" + pokemon + ".png");
            texthelft.setAttribute("value", "Kijk dan weer naar links");
            texthelft.setAttribute("position", "-1 3 -4");
          }
          else{
            box.setAttribute("src", urlL[currentPokemon]);
            currentPokemon = 4;
            menu.setAttribute("material", "opacity: 0.5");
            texthelft.setAttribute("value", "De battle is begonnen!");
            texthelft.setAttribute("position", "-1 3 -4")
            let acties = actie.split(' ');
            for (let j = 0; j< 4; j++){
              if (j == 0){
                actie0.setAttribute("value", acties[j]);
              }
              else if (j == 1){
                actie1.setAttribute("value", acties[j]);
              }
              else if (j == 2){
                actie2.setAttribute("value", acties[j]);
              }
              else {
                actie3.setAttribute("value", acties[j]);
              }
            };
          }
          box.setAttribute("position", {x: this.getAttribute("position").x, y: 0.5, z: this.getAttribute("position").z});
          scene.appendChild(box);
          addListeners();
          hold = null;
          document.getElementById("js--hold").remove();
          pokemon = '';
        }
      });
    }

  //4 random pokemons
  for (let i = 0; i < hussel.length; i++){
      hussel[i].addEventListener('click', function(evt){
        urlL = [];
        for(let i = 0; i < pokemonImages.length; i++){
            let number = Math.floor(Math.random() * 150 + 1);
              getPokemon(number,pokemonImages[i],i);
      }
      texteind.setAttribute("value", "Kijk weer naar rechts!")
    });
  }

const getPokemon = (number,pokemonImage,i) => {
    let pokemon = fetch("https://pokeapi.co/api/v2/pokemon/" + number)
      .then((data) => {
          return data.json();
      })
      .then((response) => {
        currentPokemon = i;
        if (i == 3){
          urlL.splice(i, 0, response.sprites.front_shiny);
          changePokemon(response.sprites.front_shiny,pokemonImage);}
        else{
          urlL.splice(i, 0, response.sprites.front_default);
          changePokemon(response.sprites.front_default,pokemonImage);}
      });
  }
  const changePokemon = (pokemonSprite,pokemonImage) => {
    pokemonImage.setAttribute("src",pokemonSprite);
  }
}
